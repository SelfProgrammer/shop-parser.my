<?php

namespace App\Models;

use App\Core\Classes\Model;

class FeatureValue extends Model
{
    public $table = 'features_values';
    
    public function attributes()
    {
        return [
            'id',
            'feature_id',
            'name',
        ];
    }
}
