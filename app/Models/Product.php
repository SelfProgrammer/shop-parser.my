<?php

namespace App\Models;

use App\Core\Classes\Model;

class Product extends Model
{
    public function attributes()
    {
        return [
            'id',
            'name',
            'target_link',
            'code',
        ];
    }
}
