<?php

namespace App\Models;

use App\Core\Classes\Model;

class Feature extends Model
{
    public function attributes()
    {
        return [
            'id',
            'name',
        ];
    }
}
