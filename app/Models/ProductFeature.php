<?php

namespace App\Models;

use App\Core\Classes\Model;

class ProductFeature extends Model
{
    public $table = 'products_features';
    
    public function attributes()
    {
        return [
            'id',
            'product_id',
            'feature_id',
            'value_id',
        ];
    }
}
