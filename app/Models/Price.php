<?php

namespace App\Models;

use App\Core\Classes\Model;

class Price extends Model
{
    public function attributes()
    {
        return [
            'id',
            'product_id',
            'price',
            'old_price',
        ];
    }
}
