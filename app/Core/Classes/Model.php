<?php

namespace App\Core\Classes;

use App\Core\Connection\DB;

abstract class Model 
{
    protected $connection;
    
    public $attributes = [];
    
    public $table;
    
    public function __construct() 
    {
        $this->connection = new DB();
        
        if (!$this->table) {
            $this->table = strtolower((new \ReflectionClass($this))->getShortName()) . 's';
        }
        
        $this->fill();
    }
    
    public function fill(array $attributes = []) 
    {
        if (empty($attributes)) {
            $this->attributes = array_fill_keys(array_keys(array_flip($this->attributes())), null);
        } else {
            foreach ($attributes as $key => $value) {
                if (array_key_exists($key, $this->attributes)) {
                    $this->attributes[$key] = $value;
                }
            }
        }
    }
    
    abstract public function attributes();
    
    public function __get($property)
    {
        if (array_key_exists($property, $this->attributes)) {
            return $this->attributes[$property];
        }
        return null;
    }
 
    public function __set($property, $value)
    {
        if (array_key_exists($property, $this->attributes)) {
            $this->attributes[$property] = $value;
            return;
        }
        return null;
    }
    
    public static function findOne(array $params) 
    {
        $model = new static();
        $where = [];
        foreach ($params as $key => $value) {
            $item = '`' . $key . '`=';
            $item .= $value ? (is_string($value) ? "'" . $value . "'" : $value) : 'NULL';
            $where[] = $item;
        }
        
        $where = implode(' AND ', $where);
        
        $query = "SELECT * FROM `".$model->table."` WHERE $where";
        $res = $model->connection->query($query);
        if (is_array($res) && !empty($res['status']) && !$res['status']) {
            echo $res['msg'] . "\n";
        } else {
            $result = $model->connection->fetchArray();
            if (!empty($result)) {
                $model->fill($result);
                return $model;
            }
        }
        
        return null;
    }
    
    public static function findAll(array $params) 
    {
        // TODO
    }
    
    public static function findWhereNotIn(array $params, string $notInField, array $notInValues) 
    {
        $model = new static();
        $where = [];
        foreach ($params as $key => $value) {
            $item = '`' . $key . '`=';
            $item .= $value ? (is_string($value) ? "'" . $value . "'" : $value) : 'NULL';
            $where[] = $item;
        }
        
        $where = implode(' AND ', $where);
        
        $notIn = [];
        foreach ($notInValues as $notInValue) {
            $notIn[] = is_string($notInValue) ? "'" . $notInValue . "'" : $notInValue;
        }
        
        $notIn = implode(',', $notIn);
        
        $query = "SELECT * FROM `".$model->table."` WHERE $where AND $notInField NOT IN ($notIn)";
        $res = $model->connection->query($query);
        $results = [];
        
        if (is_array($res) && !empty($res['status']) && !$res['status']) {
            echo $res['msg'] . "\n";
        } else {
            $rows = $model->connection->fetchAll();
            foreach ($rows as $row) {
                $model = new static();
                $model->fill($row);
                $results[] = $model;
            }
        }
       
        return $results;
    }
    
    public function delete() 
    {
        $query = "DELETE FROM `".$this->table."` WHERE `id` = " . $this->id;
        $res = $this->connection->query($query);
        if (is_array($res) && !empty($res['status']) && !$res['status']) {
            echo $res['msg'] . "\n";
        }
    }
    
    public function save() 
    {
        if (!is_null($this->id)) {
            return $this->update($this->attributes);
        } else {
            return $this->insert($this->attributes);
        }
    }
    
    protected function update($attributes)
    {
        $id = $this->id;
        unset($attributes['id']);
        
        $args = [];
        foreach ($attributes as $key => $value) {
            $item = '`' . $key . '`=';
            $item .= $value ? (is_string($value) ? "'" . $value . "'" : $value) : 'NULL';
            $args[] = $item;
        }
        $args = implode(',', $args);
        $query = "UPDATE `".$this->table."` SET $args WHERE `id` = $id";
        $res = $this->connection->query($query);
        
        if (is_array($res) && !empty($res['status']) && !$res['status']) {
            echo $res['msg'] . "\n";
            return false;
        } else {
            return true;
        }
    }
    
    protected function insert($attributes)
    {
        $keys = $values = [];
        foreach ($attributes as $key => $value) {
            $keys[] = '`' . $key . '`';
            $values[] = $value ? (is_string($value) ? "'" . $value . "'" : $value) : 'NULL';
        }
        $values = implode(',', $values);
        $keys = implode(',', $keys);
        $query = "INSERT INTO `".$this->table."` ($keys) VALUES ($values)";
        $res = $this->connection->query($query);
        if (is_array($res) && !empty($res['status']) && !$res['status']) {
            echo $res['msg'] . "\n";
            return false;
        } else {
            $this->id = $this->connection->lastInsertID();
            return true;
        }
    }

}
