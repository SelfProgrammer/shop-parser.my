<?php

namespace App\Parsers;

use App\Models\Product;
use App\Models\Price;
use App\Models\Feature;
use App\Models\FeatureValue;
use App\Models\ProductFeature;

class Foxtrot 
{
    protected $url;
    protected $maxPage = 1;
    protected $pagesLoaded = false;
    protected $baseUrl = 'https://www.foxtrot.com.ua';
    
    protected $total = 0;
    protected $itemsAdded = 0;
    protected $itemsUpdated = 0;
    
    
    public function __construct(string $url) {
        $this->url = $url;
        
        include_once('App\Libs\simple_html_dom.php');
        set_time_limit(0);
    }
    
    public function run()
    {
        echo "Start parcing \n";
        $currentPage = 1;
        do {
            $url = $this->url;
            if ($currentPage > 1) {
                $url .= '?page=' . $currentPage;
            }
            $src = $this->file_get_contents_curl($url);
            $html = str_get_html($src);
            if (!$this->pagesLoaded) {
                $this->loadNumPages($html);
            }
            $this->parsePage($html);
            $currentPage++;
        } while ($currentPage <= $this->maxPage);
        
        $str = "Done! \n";
        $str .= "Total items: " . $this->total . "\n";
        $str .= "New added items: " . $this->itemsAdded . "\n";
        $str .= "Updated items: " . ($this->itemsUpdated - $this->itemsAdded) . "\n";
        $str .= "Failed items: " . ($this->total - $this->itemsUpdated) . "\n";
        $str .= "More details by failed items see at logs/error_log.txt\n";
        echo $str;
        $logName = 'parcing_log_' . date('Y-m-d_H-i-s') . '.txt';
        file_put_contents('logs/'.$logName, $str);
    }
    
    protected function loadNumPages($html) 
    {
        $maxPage = $this->maxPage;
        foreach($html->find('div[class="listing__pagination"] li a') as $link) {
            $page = (int)trim($link->plaintext);
            if ($page > $maxPage) {
                $maxPage = $page;
            }
        }
        $this->maxPage = $maxPage;
        $this->pagesLoaded = true;
    }
    
    protected function parsePage($html) 
    {
        foreach($html->find('div[class="listing__body-wrap"] div[class="card__body"] a[class="card__title"]') as $productLink) {
            $this->total++;
            echo 'Progress: ' . $this->total . "\n";
            $productData = $this->parseProductPage($productLink->attr['href']);
            if (!$productData) {
                continue;
            }
            $this->saveProductData($productData);
        }
    }
    
    protected function parseProductPage($link) 
    {
        $src = $this->file_get_contents_curl($this->baseUrl . $link);
        $html = str_get_html($src);
        
        $data = [];
        $data['title'] = trim($html->find('h1[class="page__title"]')[0]->plaintext);
        if (empty($data['title'])) {
            $msg = date("Y-m-d H:i:s") . ' | empty name for url '.$this->baseUrl . $link."\n";
            error_log($msg, 3, 'logs\error_log.txt');
            return false;
        }
        $data['link'] = $this->baseUrl . $link;
        $data['code'] = trim(str_replace('Код:', '', $html->find('div[class="product-box__main-code"]')[0]->plaintext));
        if (empty($data['code'])) {
            $msg = date("Y-m-d H:i:s") . ' | empty vendor code for url '.$this->baseUrl . $link."\n";
            error_log($msg, 3, 'logs\error_log.txt');
            return false;
        }
        
        $data['price'] = trim(str_replace(['₴', ' '], '', $html->find('div[class="card-price"]')[0]->plaintext));
        $data['old_price'] = trim(str_replace([' '], '', $html->find('div[class="product-box__main-price__discount"] p')[0]->plaintext));
        
        $data['features'] = [];
        $attrLines = $html->find('div[id="product-specs-popup"] table[class="popup-table_v2"] tr');
        foreach($attrLines as $attrLine) {
            
            $data['features'][trim($attrLine->children[0]->plaintext)] = array_map(function($item){
                return trim($item);
            }, explode(';', str_replace(['&quot;', '&#39;'], ['"', "'"], $attrLine->children[1]->plaintext)));
        }

        return $data;
    }
    
    protected function file_get_contents_curl($url) 
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36';
        $headers[] = 'Cookie: __RequestVerificationToken=K1wqHvBMgj1Ob201ycX0FlXRHqKBXYoRjHaEII4fhI--qlXPRNGal13vhVDHm-l35wGBYEEaWGPsD-xMRZz32NK8umg1; sc=DDD91278-E1B1-ADB2-3434-4ECA5C3E4AC1; _ga=GA1.3.833111648.1576397388; cto_lwid=ab8854aa-c74b-46aa-9be3-1eb351bb0f5e; _fbp=fb.2.1576397388535.1588821243; v_cnt=2; MG_page_count=2; visid_incap_2265310=2i8H1uZBQBCosXagpVzL+RUylF4AAAAAQUIPAAAAAABheMiBW3xOACuX88MeG5z/; nlbi_2265310=KiCiJgknXU6oiJwPfdLtaQAAAACLbfwP7sELWrvvz7yscszu; ft_user=d49dfe77-97d9-4cd0-89a4-f79f9c89c12a; _gid=GA1.3.1916067356.1586770460; ft_recently=6552327,6386384,6319248,; lang=ru; incap_ses_259_2265310=hrY2e4iUY3Dur7nsLimYA2yWlV4AAAAAXTqnXWONOQYI9sc4uXLJVw==; fp=32; lfp=4/13/2020, 12:34:17 PM; pa=1586861714633.89920.6970214899558687www.foxtrot.com.ua0.19596414568092269+2; _gat_UA-20792810-10=1';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        
        $responseInfo = curl_getinfo($ch);
        if ($responseInfo['http_code'] !== 200) {
            $msg = date("Y-m-d H:i:s") . ' | unexpected error. response status '.$responseInfo['http_code'].' for url '.$url."\n";
            error_log($msg, 3, 'logs\error_log.txt');
            if ($responseInfo['http_code'] === 503) {
                $msg = 'command finished with response status '.$responseInfo['http_code'].' for url '.$url;
                error_log($msg, 1, ADMIN_MAIL);
            }
        }
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }
    
    protected function saveProductData($data) 
    {
        $process = true;
        $product = Product::findOne([
            'target_link' => $data['link'],
        ]);
        if (!$product) {
            $product = new Product;
            $product->name = $data['title'];
            $product->target_link = $data['link'];
            $product->code = $data['code'];
            $res = $product->save();
            if ($res) {
                $this->itemsAdded++;
            } else {
                $process = false;
            }
        }
        
        if ($process) {
            $productPrice = Price::findOne([
                'product_id' => $product->id,
            ]);
            if (!$productPrice) {
                $productPrice = new Price;
                $productPrice->product_id = $product->id;
                $productPrice->price = $data['price'];
                $productPrice->old_price = $data['old_price'];
                $productPrice->save();
            }

            $productFetureValues = [];
            foreach ($data['features'] as $key => $values) {
                $feature = Feature::findOne([
                    'name' => $key,
                ]);
                if (!$feature) {
                    $feature = new Feature;
                    $feature->name = $key;
                    if (!$feature->save()) {
                        continue;
                    } 
                }
                foreach ($values as $value) {
                    $featureValue = FeatureValue::findOne([
                        'feature_id' => $feature->id,
                        'name' => $value,
                    ]);
                    if (!$featureValue) {
                        $featureValue = new FeatureValue();
                        $featureValue->feature_id = $feature->id;
                        $featureValue->name = $value;
                        $featureValue->save();
                    }

                    $productFeature = ProductFeature::findOne([
                        'feature_id' => $feature->id,
                        'value_id' => $featureValue->id,
                        'product_id' => $product->id,
                    ]);
                    if (!$productFeature) {
                        $productFeature = new ProductFeature();
                        $productFeature->product_id = $product->id;
                        $productFeature->feature_id = $feature->id;
                        $productFeature->value_id = $featureValue->id;
                        $productFeature->save();
                    }
                    $productFetureValues[] = $featureValue->id;
                }
            }
            $oldFeatures = ProductFeature::findWhereNotIn([
                'product_id' => $product->id,
            ], 'value_id', $productFetureValues);

            foreach ($oldFeatures as $oldFeature) {
                $oldFeature->delete();
            }
        }
        $this->itemsUpdated++;
    }
    
}
