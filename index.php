<?php

use App\Parsers\Foxtrot;

require_once 'vendor/autoload.php';

$url = readline("Enter a link to parse: ");

while (empty($url)) {
    $url = readline("No no no... Please enter a link: ");
}

$parser = new Foxtrot($url);
$parser->run();
